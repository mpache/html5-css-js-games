/*
 * Copyright 2013 Adrián López González <adrian.lg.456@gmail.com>
 *
 * This file is part of pingPongV2.
 *
 * pingPongV2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * pingPongV2 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with pingPongV2.  If not, see <http://www.gnu.org/licenses/>.
 */
//========
// FIELDS
//========
// Initialize canvas and create a context 2d.
var canvas = document.getElementById("canvas");
var ctx = canvas.getContext("2d");
// Screen measures
var screenWidth = canvas.width;
var screenHeight = canvas.height;
// Elements into screen
var points = 0;
var paddles = new Array();
// State of game.
var isGameOver = false; 

// Variable to recall every time to animate game.
var init;

// Necessary Objects. NOTE: is to put listeners to mouse.
var mouse = {};


//=========
// OBJECTS
//=========
// Ball object.
var ball = {
	// Fields
	posX: screenWidth / 2,
	posY: screenHeight / 3,
	radius: 5,
	color: "black",
	moveX: 4, 
	moveY: 8,
	// Functions
	draw: function() {
		ctx.beginPath();
		ctx.fillStyle = this.color;
		ctx.arc(this.posX, this.posY, this.radius, 0, Math.PI * 2, false);
		ctx.fill();
	},
};

// Paddle object
var Paddle = function(pos) {
	this.width = 150;	
	this.height = 5;
	this.posX = screenWidth / 2 - this.width / 2; 
	this.posY = pos === "top" ? 0 : screenHeight -this.height; 
}

// Buttons
//----------------
// Start button object
var startButton = {
	// Fields
	width: 100,
	height: 50,
	posX: screenWidth / 2 - 50, 
	posY: screenHeight / 2 - 25, 
	// Functions
	draw: function() {
		// Create Rectangle
		ctx.strokeStyle = "black";
		ctx.lineWidth = "5";
		ctx.strokeRect(this.posX, this.posY, this.width, this.height);	
		// Create font
		ctx.font = "20px cursive";
		ctx.textAlign = "center";
		ctx.textBaseline = "middle";
		ctx.fillStlye = "black";
		ctx.fillText("Start", screenWidth / 2, screenHeight /2); 
	}
};
// Restart button object
var restartButton = {
	// Fields
	width: 100,
	height: 50,
	posX: screenWidth / 2 - 50, 
	posY: screenHeight / 2 - 25,
	// Functions
	draw: function() {
		// Create Rectangle
		ctx.strokeStyle = "black";
		ctx.lineWidth = "5";
		ctx.strokeRect(this.posX, this.posY, this.width, this.height);	
		// Create font
		ctx.font = "20px cursive";
		ctx.textAlign = "center";
		ctx.textBaseline = "middle";
		ctx.fillStlye = "black";
		ctx.fillText("Restart", screenWidth / 2, screenHeight / 2); 
	}
};

//===========
// FUNCTIONS
//===========
// MODEL
//-------
// Function to increase speed after every 5 points.
function increaseSpeed() {
	if(points % 5 == 0) {
		ball.moveX += ball.moveX < 0 ? -1 : 1;
		ball.moveY += ball.moveY < 0 ? -2 : 2;
	}
}

// Function that check if there are a collision of paddles with ball.
function thereCollisionBallPaddle(paddle) {
	// Check if ball is Xaxis of paddle
	if (ball.posX + ball.radius >= paddle.posX && ball.posX - ball.radius <= paddle.posX + paddle.width) {
		// Check if ball touch paddle bottom.
		if (ball.posY >= (paddle.posY - paddle.height) && paddle.posY > 0) {
			return true;
		} 
		// Check if ball touch paddle top.
		else if (ball.posY <= paddle.height && paddle.posY == 0) {
			return true;
		}
		// Ball not collide by the paddles.
		else {
			return false;
		}
	} 
}

// Update all elements of screen.
function update() {
	// Update score.
	putScore();

	// Move the paddle controlled by IA (top).
	paddles[0].posX = ball.posX - paddles[0].width / 2;
	// Move paddle controlled by user (bottom). NOTE: move paddle on mouse move.
	if(mouse.x) {
		paddles[1].posX = mouse.x -  paddles[1].width / 2;	
	}

	// Move the ball
	ball.posX += ball.moveX;
	ball.posY += ball.moveY;

	// Check if there are a collision of paddles with ball.
	if (thereCollisionBallPaddle(paddles[0])) {
		ball.moveY = -ball.moveY;
	} else if (thereCollisionBallPaddle(paddles[1])) {
		ball.moveY = -ball.moveY;
		points++;
		increaseSpeed();
	} else {
		// Check the collision with screen width.
		if (ball.posX + ball.radius > screenWidth) {
			ball.moveX = -ball.moveX;
			ball.posX = screenWidth - ball.radius;
		} else if (ball.posX - ball.radius < 0) {
			ball.moveX = -ball.moveX;
			ball.posX = ball.radius;
		}			
		// If not are a collision, check if game is over.
		if (ball.posY + ball.radius > screenHeight) {
			ball.posY = screenHeight;
			gameOver();
		} else if (ball.posY < 0) {
			ball.posY = 0;
			gameOver();		
		}
	}
}

// VIEW
//------
// Function to paint canvas.
function paintCanvas() {
	ctx.fillStyle = "white";
	ctx.fillRect(0, 0, screenWidth, screenHeight);
}

// Draw elements into screen.
function draw() {
	paintCanvas();
	// Draw paddles.
	for (i in paddles) {
		paddle = paddles[i];
		ctx.fillStyle = "black";
		ctx.fillRect(paddle.posX, paddle.posY, paddle.width, paddle.height);
	}
	// draw ball.
	ball.draw();
	// Update elements.
	update();
}

// Function for updating score
function putScore() {
	ctx.fillStlye = "black";
	ctx.font = "16px cursive";
	ctx.textAlign = "left";
	ctx.textBaseline = "top";
	ctx.fillText("Score: " + points, 20, 20 );
}

// Function to run when the game is over.
function gameOver() {
	// Change state of game to gameOver.
	isGameOver = true;
	// Info player that the game is game over and put your score.
	ctx.fillStlye = "black";
	ctx.font = "20px cursive";
	ctx.textAlign = "center";
	ctx.textBaseline = "middle";
	ctx.fillText("Game Over!!! " + points + " points!!", screenWidth / 2, screenHeight / 2 + 50); 
	// Show the restart button
	restartButton.draw();
	// Stop the Animation
	clearTimeout(init);
}

// Render!!! NOTE: updates itself using the timing.
function render() {
	init = setTimeout('render()', 15);
	draw();
}

//=========== 
// LISTENERS
//===========
function trackPosition(e) {
	mouse.x = e.pageX - screenWidth / 2 - 50;
}

// On button click (Restart and start)
function buttonClick(e) {
	// Variables for storing mouse position on click
	var mouseX = e.pageX - screenWidth / 2 - 40;
	var mouseY = screenHeight + 10 - e.pageY;
	// Click start button
	if(mouseX >= startButton.posX && mouseX <= startButton.posX + startButton.width) {
		render();
		// Delete the start button after clicking it
		startButton = {};
	}
	// If the game is gameOver, and the restart button is clicked
	if(isGameOver) {
		if(mouseX >= restartButton.posX && mouseX <= restartButton.posX + restartButton.width) {
			ball.posX = 20;
			ball.posY = 20;
			points = 0;
			ball.moveX = 4;
			ball.moveY = 8;
			isGameOver = false;
			render();
		}
	}
}

// Add mousemove and mousedown listeners to the canvas
canvas.addEventListener("mousemove", trackPosition, true);
canvas.addEventListener("mousedown", buttonClick, true);

//======
// MAIN
//======
function start() {
	// Create paddles and put in the array.
	paddles.push(new Paddle("top"));
	paddles.push(new Paddle("bottom"));
	// Draw elements on screen and button start.
	draw();
	startButton.draw();
}

// Initialized application
start();
