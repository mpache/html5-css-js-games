/*
 * Copyright 2013 Adrián López González <adrian.lg.456@gmail.com>
 *
 * This file is part of BattleShips.
 *
 * BattleShips is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BattleShips is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BattleShips.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * SplashScreen.js
 * 
 * @version 1.0
 * @author Adrián López González
 * 
 */

function SplashScreen() {
	
	AScreen.call(this, arguments[0]);

	/* Fields */
	var isFill = true;

	/**
	 * Animation screen.
	 */
	this.animation = function() {
		var splashScreen = document.getElementById("splashScreen");
		if (this.getIsAnimationComplete()) {
			splashScreen.style.opacity = 0.0;
			splashScreen.style.display = 'none';
			// Call intro main menu animation.
			this.setIsAnimationComplete(false);
			// Change screen.
			var controller = this.getController();
			controller.setNextScreen(new MainMenuScreen(controller));
		} else {
			// Create animation splash screen.
			splashScreen.style.display = 'inline';
			var opacity = splashScreen.style.opacity == '' ? 0.0
					: splashScreen.style.opacity * 1;
			// Configure animation.
			if (isFill && opacity < 1.0) {
				opacity += 0.01;
				splashScreen.style.opacity = opacity.toFixed(2);
			} else {
				isFill = false;
			}
			if (!isFill && opacity > 0.0) {
				opacity -= 0.1;
				splashScreen.style.opacity = opacity.toFixed(2);
			} else if (!isFill) {
				this.setIsAnimationComplete(true);
			}
		}
	};
}

// Inherit from AScreen.
SplashScreen.prototype = new AScreen();
