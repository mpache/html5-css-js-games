/*
 * Copyright 2013 Adrián López González <adrian.lg.456@gmail.com>
 *
 * This file is part of BattleShips.
 *
 * BattleShips is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BattleShips is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BattleShips.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * MainMenuScreen.js
 * 
 * @version 1.0
 * @author Adrián López González
 * 
 */

function MainMenuScreen() {
	AScreen.call(this, arguments[0]);

	/* Fields */
	var context = null;
	var isPlayPush = false;
	var isCreditsPush = false;

	/**
	 * Animation screen.
	 */
	this.animation = function() {
		if (context == null)
			context = this;
		if (!this.getIsAnimationComplete()) {
			if (!isPlayPush && !isCreditsPush)
				introMainMenu();
			else
				animationMainMenuReverse();
		} else if (this.getIsAnimationComplete() && isPlayPush) {
			var controller = this.getController();
			controller.setNextScreen(new GameLoopScreen(controller));
		}
	};

	/**
	 * Play game.
	 */
	this.play = function() {
		if (context.getIsAnimationComplete()) {
			context.setIsAnimationComplete(false);
			isPlayPush = true;
			isCreditsPush = false;
		}
	};

	/**
	 * Credits function.
	 */
	this.showMainMenuCredits = function() {
		if (context.getIsAnimationComplete()) {
			context.setIsAnimationComplete(false);
			isPlayPush = false;
			isCreditsPush = true;
		}
	};

	/**
	 * Close pop up credits.
	 */
	this.closeMainMenuCredits = function() {
		if (context.getIsAnimationComplete()) {
			context.setIsAnimationComplete(false);
			isPlayPush = false;
			isCreditsPush = false;
		}
	};

	// Put events.
	document.getElementById("bPlay").onclick = this.play;
	document.getElementById("bOpenCredits").onclick = this.showMainMenuCredits;
	document.getElementById("bCloseCredits").onclick = this.closeMainMenuCredits;
	
	// Private methods.
	var introMainMenu = function() {
		var mainMenuScreen = document.getElementById("mainMenuScreen");
		mainMenuScreen.style.display = 'inline';
		// Title
		var mainMenuTitle = document.getElementById("mainMenuTitle");
		var marginTopMainMenuTitle = mainMenuTitle.style.marginTop == '' ? -500
				: parseInt(mainMenuTitle.style.marginTop);
		// Main menu buttons
		var mainMenuButtons = document.getElementById("mainMenuButtons");
		var opacityMainMenuButtons = mainMenuButtons.style.opacity == '' ? 0.0
				: mainMenuButtons.style.opacity * 1;
		// Main menu credits.
		var mainMenuCredits = document.getElementById("mainMenuCredits");
		var opacityMainMenuCredits = mainMenuCredits.style.opacity == '' ? 0.0
				: mainMenuCredits.style.opacity * 1;
		// Create animation.
		if (opacityMainMenuButtons < 1.0 || marginTopMainMenuTitle < 0) {
			// Main menu title
			marginTopMainMenuTitle = marginTopMainMenuTitle < 0 ? marginTopMainMenuTitle + 25
					: 0;
			mainMenuTitle.style.marginTop = marginTopMainMenuTitle + "px";
			// Main menu buttons
			opacityMainMenuButtons += 0.04;
			mainMenuButtons.style.opacity = opacityMainMenuButtons.toFixed(2);
			// Main menu credits.
			opacityMainMenuCredits = opacityMainMenuCredits == 0.0 ? 0
					: opacityMainMenuCredits - 0.1;
			mainMenuCredits.style.opacity = opacityMainMenuCredits.toFixed(2);
		} else {
			mainMenuCredits.style.display = 'none';
			context.setIsAnimationComplete(true);
		}
	};

	var animationMainMenuReverse = function() {
		var mainMenuScreen = document.getElementById("mainMenuScreen");
		// Title
		var mainMenuTitle = document.getElementById("mainMenuTitle");
		var marginTopMainMenuTitle = parseInt(mainMenuTitle.style.marginTop);
		// Main menu buttons
		var mainMenuButtons = document.getElementById("mainMenuButtons");
		var opacity = mainMenuButtons.style.opacity * 1;
		// Create animation.
		if (opacity > 0.0 || marginTopMainMenuTitle > -500) {
			marginTopMainMenuTitle = marginTopMainMenuTitle > -500 ? marginTopMainMenuTitle - 25
					: -500;
			mainMenuTitle.style.marginTop = marginTopMainMenuTitle + "px";
			opacity -= 0.04;
			mainMenuButtons.style.opacity = opacity.toFixed(2);
		} else {
			if (isCreditsPush) {
				// Main menu credits.
				var mainMenuCredits = document
						.getElementById("mainMenuCredits");
				mainMenuCredits.style.display = 'inline';
				opacity = mainMenuCredits.style.opacity * 1;
				if (opacity < 1.0) {
					opacity += 0.1;
					mainMenuCredits.style.opacity = opacity.toFixed(2);
				} else {
					context.setIsAnimationComplete(true);
				}
			} else if (isPlayPush) {
				mainMenuScreen.style.display = 'none';
				context.setIsAnimationComplete(true);
			}
		}
	};
}

// Inherit from AScreen.
MainMenuScreen.prototype = new AScreen();
