/*
 * Copyright 2013 Adrián López González <adrian.lg.456@gmail.com>
 *
 * This file is part of BattleShips.
 *
 * BattleShips is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BattleShips is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BattleShips.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * GameLoopScreen.js
 * 
 * @version 1.0
 * @author Adrián López González
 * 
 */

function GameLoopScreen() {
	AScreen.call(this, arguments[0]);

	/* Fields */
	var context = null;
	var world = null;

	/**
	 * Animation screen.
	 */
	this.animation = function() {
		if (context == null) {
			context = this;
			world = null;
			this.showGameLoop();
		}
		if (world != null)
			try {
				this.checkIfGameIsOver();
			} catch (error) {
				// Nop
			}
	};

	/**
	 * Function to return main menu
	 */
	this.returnMainMenu = function() {
		// Hide pause menu.
		context.pauseOrResumeGame();
		context.restartGame();
		// Hide gameLoop.
		var gameLoop = document.getElementById("gameLoop");
		gameLoop.style.display = 'none';
		// Return main menu Screen
		context.setIsAnimationComplete(false);
		var controller = context.getController();
		controller.setNextScreen(new MainMenuScreen(controller));
	};

	/**
	 * function to restart game.
	 */
	this.restartGame = function() {
		document.getElementById("gameOver").style.display = "none";
		document.getElementById("locateShipsPanel").style.display = "inline";
		document.getElementById("startGame").style.display = "none";
		context.showGameLoop();
	};

	/**
	 * Function to show game loop.
	 */
	this.showGameLoop = function() {
		document.getElementById("gameLoop").style.display = "inline";
		// Check if the fist time of play game.
		if (world == null) {
			// Create new world.
			world = new World();
			// Show how to play.
			this.showOrCloseHowToPlay();
		} else {
			document.getElementById("locateShipsPanel").style.display = "inline";
			document.getElementById("startGame").style.display = "none";
			document.getElementById("gameOver").style.display = "none";
		}
		world.isGameEnd = false;
		world.isGameStart = false;
		world.isGamePause = true;
		// Create panels.
		context.createPanels(world.getEnemyIA());
		context.createPanels(world.getUser());
		// Positioned ships of enemy randomly.
		world.getEnemyIA().positionedShipsRandomly();
	};

	/**
	 * Put ships user random.
	 */
	this.putShipsUserRandom = function() {
		if (document.getElementById("howToPlay").style.display == "none") {
			document.getElementById("startGame").style.display = "inline";
			context.createPanels(world.getUser());
			world.getUser().positionedShipsRandomly();
			context.showShips(world.getUser());
		}
	};

	/**
	 * Function to start Game.
	 */
	this.startGame = function() {
		document.getElementById("locateShipsPanel").style.display = "none";
		world.isGamePause = false;
		world.isGameStart = true;
		// Launch interceptor app to determine if game is over.
		context.checkIfGameIsOver();
	};

	/**
	 * Function to check in a time with player is winner.
	 */
	this.checkIfGameIsOver = function() {
		if (world.getUser().areLoose() || world.getEnemyIA().areLoose()) {
			world.isGameEnd = true;
			document.getElementById("gameOver").style.display = "inline";
			// Show ships enemy.
			context.showShips(world.getEnemyIA());
			// Put a win or lose to user.
			var p = document.getElementById("gameOver").childNodes;
			if (!world.getUser().areLoose()) {
				p.item(1).innerHTML = "<b>Game Over, You Win!" + "</b>";
			} else {
				p.item(1).innerHTML = "<b>Game Over, You Loose!" + "</b>";
			}
		}
	};

	/**
	 * Function to pause or resume game.
	 */
	this.pauseOrResumeGame = function() {
		world.isGamePause = true;
		var howToPlay = document.getElementById("howToPlay");
		if (howToPlay.style.display == 'none' && !world.isIACalculatingMove
				&& !world.isGameEnd) {
			var pauseGame = document.getElementById("pauseGame");
			if (pauseGame.style.display == 'none'
					|| pauseGame.style.display == '') {
				pauseGame.style.display = 'inline';
			} else {
				pauseGame.style.display = 'none';
				world.isGamePause = false;
			}
		}
	};

	/**
	 * Show or close how to play.
	 */
	this.showOrCloseHowToPlay = function() {
		world.isGamePause = true;
		var howToPlay = document.getElementById("howToPlay");
		if (howToPlay.style.display == 'none' || howToPlay.style.display == '') {
			howToPlay.style.display = 'inline';
			document.getElementById("pauseGame").style.display = 'none';
			document.getElementById("howToPlayPage1").style.display = "inline";
			document.getElementById("leftArrow").style.display = "none";
			document.getElementById("howToPlayPage2").style.display = "none";
			document.getElementById("howToPlayPage3").style.display = "none";
			document.getElementById("rightArrow").style.display = "inline";
		} else {
			howToPlay.style.display = 'none';
			world.isGamePause = false;
		}
	};

	/**
	 * Function used in how to play to go previous page.
	 */
	this.prevPage = function() {
		if (document.getElementById("howToPlayPage2").style.display == "inline") {
			document.getElementById("howToPlayPage1").style.display = "inline";
			document.getElementById("howToPlayPage2").style.display = "none";
			document.getElementById("leftArrow").style.display = "none";
		} else if (document.getElementById("howToPlayPage3").style.display == "inline") {
			document.getElementById("howToPlayPage3").style.display = "none";
			document.getElementById("howToPlayPage2").style.display = "inline";
			document.getElementById("rightArrow").style.display = "inline";
		}
	};

	/**
	 * Function used in how to play to go next page.
	 */
	this.nextPage = function() {
		if (document.getElementById("howToPlayPage1").style.display == "inline") {
			document.getElementById("howToPlayPage1").style.display = "none";
			document.getElementById("howToPlayPage2").style.display = "inline";
			document.getElementById("leftArrow").style.display = "inline";
		} else if (document.getElementById("howToPlayPage2").style.display == "inline") {
			document.getElementById("howToPlayPage2").style.display = "none";
			document.getElementById("howToPlayPage3").style.display = "inline";
			document.getElementById("rightArrow").style.display = "none";
		}
	};

	/**
	 * Function to show ships in table.
	 * 
	 * @param player
	 *            indicate if player is a user or enemy.
	 */
	this.showShips = function(player) {
		var panel;
		var td;
		if (player instanceof User) {
			var panelUser = document.getElementById("panelUser");
			td = panelUser.getElementsByTagName("td");
			panel = world.getUser().getPanel();
		} else {
			var panelEnemy = document.getElementById("panelEnemy");
			td = panelEnemy.getElementsByTagName("td");
			panel = world.getEnemyIA().getPanel();
		}
		// Show panel.
		for (var i = 0; i < panel.length; i++) {
			if (panel[i] == '*') {
				td[i].style.background = 'green';
			} else if (panel[i] == 't') {
				td[i].style.background = 'red';
			}
		}
	};

	/**
	 * Function to create panels of game.
	 * 
	 * @param player
	 *            indicate if player is a user or enemy.
	 */
	this.createPanels = function(player) {
		// Initialized panel of enemy
		if (player instanceof EnemyIA) {
			var panel = world.getEnemyIA().getPanel(true);
			var panelEnemy = document.getElementById("panelEnemy");
			// Delete rows if exists any.
			while (panelEnemy.hasChildNodes()) {
				panelEnemy.removeChild(panelEnemy.firstChild);
			}
			for (var i = 0; i < panel.length; i++) {
				// Create rows.
				if (i % 20 == 0) {
					var newRow = panelEnemy.insertRow(-1);
				}
				var cell = newRow.insertCell(-1);
				cell.className = 'box';
				cell.id = i + 'e';
			}
		}
		// Initialized panel of user.
		else {
			var panel = world.getUser().getPanel(true);
			var panelUser = document.getElementById("panelUser");
			// Delete rows if exists any.
			while (panelUser.hasChildNodes()) {
				panelUser.removeChild(panelUser.firstChild);
			}
			// Create rows.
			for (var i = 0; i < panel.length; i++) {
				if (i % 20 == 0) {
					var newRow = panelUser.insertRow(-1);
				}
				var cell = newRow.insertCell(-1);
				cell.className = 'box';
				cell.id = i + 'u';
			}
		}

		// Put events to cells of enemy.
		var panelEnemy = document.getElementById("panelEnemy");
		var td = panelEnemy.getElementsByTagName("td");
		for (var i = 0; i < td.length; i++) {
			td[i].onmouseover = function() {
				if (!world.isGamePause && world.isGameStart
						&& !world.isIACalculatingMove && !world.isGameEnd) {
					if (this.style.backgroundColor != 'red'
							&& this.style.backgroundColor != 'blue') {
						this.style.background = 'rgba(200,10,10,0.6)';
					}
				}
			};
			td[i].onmouseout = function() {
				if (!world.isGamePause && world.isGameStart
						&& !world.isIACalculatingMove && !world.isGameEnd) {
					if (this.style.backgroundColor != 'red'
							&& this.style.backgroundColor != 'blue') {
						this.style.background = 'rgba(255,255,255,0.0)';
					}
				}
			};
			td[i].onclick = function() {
				if (!world.isGamePause && world.isGameStart
						&& !world.isIACalculatingMove && !world.isGameEnd) {
					if (this.style.backgroundColor != 'red'
							&& this.style.backgroundColor != 'blue') {
						var panel = world.getEnemyIA().getPanel();
						if (panel[parseInt(this.id)] == '*') {
							this.style.background = "red";
							panel[parseInt(this.id)] = 't';
							// Search ship touched to remove a life.
							var ships = world.getEnemyIA().getShips();
							for (var j = 0; j < ships.length; j++) {
								if (ships[j].checkPosition(parseInt(this.id))) {
									ships[j].removeLife();
									break;
								}
							}

						} else if (panel[parseInt(this.id)] == '#') {
							this.style.background = "blue";
							context.enemyTurn();
						}
					}
				}
			};
		}
	};

	/**
	 * Function to change turn to enemy.
	 */
	this.enemyTurn = function() {
		// Show thinking ia.
		document.getElementById("iaThink").style.display = "inline";
		world.isIACalculatingMove = true;
		var shot = world.getEnemyIA().shot();
		var panel = world.getUser().getPanel();
		// Panel User.
		var panelUser = document.getElementById("panelUser");
		var td = panelUser.getElementsByTagName("td");
		if (panel[shot] == '*') {
			td[shot].style.background = "red";
			panel[shot] = 't';
			// Search ship touched to remove a life.
			var ships = world.getUser().getShips();
			for (var i = 0; i < ships.length; i++) {
				if (ships[i].checkPosition(shot)) {
					ships[i].removeLife();
					break;
				}
			}
			// Recursivity.
			context.enemyTurn();
		} else if (panel[shot] == '#') {
			td[shot].style.background = "blue";
			panel[shot] = 'w';
			// Hide thinking ia.
			document.getElementById("iaThink").style.display = "none";
			world.isIACalculatingMove = false;
		} else {
			// Recursivity.
			context.enemyTurn();
		}
	};

	// Put events
	document.getElementById("bpause").onclick = this.pauseOrResumeGame;
	document.getElementById("bResume").onclick = this.pauseOrResumeGame;
	document.getElementById("bHowToPlay").onclick = this.showOrCloseHowToPlay;
	document.getElementById("bMainMenu1").onclick = this.returnMainMenu;
	document.getElementById("bRestart").onclick = this.restartGame;
	document.getElementById("bMainMenu2").onclick = this.returnMainMenu;
	document.getElementById("bPutShipsRandom").onclick = this.putShipsUserRandom;
	document.getElementById("startGame").onclick = this.startGame;
	document.getElementById("bCloseHowToPlay").onclick = this.showOrCloseHowToPlay;
	document.getElementById("leftArrow").onclick = this.prevPage;
	document.getElementById("rightArrow").onclick = this.nextPage;
}

// Inherit from AScreen.
MainMenuScreen.prototype = new AScreen();