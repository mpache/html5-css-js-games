/*
 * Copyright 2013 Adrián López González <adrian.lg.456@gmail.com>
 *
 * This file is part of BattleShips.
 *
 * BattleShips is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BattleShips is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BattleShips.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * AScreen.js
 * 
 * @version 1.0
 * @author Adrián López González
 * 
 */

function AScreen() {

	/* Fields */
	var controller = arguments[0];
	var isAnimationComplete = false;

	/**
	 * Get controller.
	 */
	this.getController = function() {
		return controller;
	};
	
	/**
 	 * Get is animation is complete.
	 */
	this.getIsAnimationComplete = function() {
		return isAnimationComplete;
	};

	/**
	 * Set is animation is complete.
	 */
	this.setIsAnimationComplete = function(isComplete) {
		isAnimationComplete = isComplete;
	};

	/**
	 * Animation screen.
	 */
	this.animation = function() {
		// Nop.
	};
}
