/*
 * Copyright 2013 Adrián López González <adrian.lg.456@gmail.com>
 *
 * This file is part of BattleShips.
 *
 * BattleShips is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BattleShips is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BattleShips.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * AShip.js
 * 
 * @version 1.0
 * @author Adrián López González
 * 
 */

function AShip(shipLength) {

	/* Fields */
	var life = shipLength;
	var length = shipLength;
	var positions = new Array(shipLength);

	/**
	 * Get life.
	 */
	this.getLife = function() {
		return life;
	};

	/**
	 * Remove life.
	 */
	this.removeLife = function() {
		life--;
	};

	/**
	 * Get length of ship.
	 */
	this.getShipLength = function() {
		return length;
	};

	/**
	 * Check position of ship.
	 */
	this.checkPosition = function(position) {
		for (var i = 0; i < positions.length; i++) {
			if (positions[i] == position) {
				return true;
			}
		}
		return false;
	};

	/**
	 * Set positions.
	 */
	this.setPositions = function(newPositions) {
		positions = newPositions;
	};
}
