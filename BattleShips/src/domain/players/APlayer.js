/*
 * Copyright 2013 Adrián López González <adrian.lg.456@gmail.com>
 *
 * This file is part of BattleShips.
 *
 * BattleShips is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BattleShips is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BattleShips.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * APlayer.js
 * 
 * @version 1.0
 * @author Adrián López González
 * 
 */

function APlayer() {

	/* Fields */
	var map = new Map();
	var ships = new Array(4);

	/**
	 * Check if player are loose.
	 */
	this.areLoose = function() {
		if (ships[0].getLife() == 0 && ships[1].getLife() == 0
				&& ships[2].getLife() == 0 && ships[3].getLife() == 0
				&& ships[4].getLife() == 0) {
			return true;
		}
		return false;
	};

	/**
	 * Get panel of map.
	 */
	this.getPanel = function(areReset) {
		if (areReset) {
			map.resetPanel();
		}
		return map.getPanel();
	};

	/**
	 * Get ships.
	 */
	this.getShips = function(areReset) {
		if (areReset) {
			ships[0] = new Flattop(5);
			ships[1] = new BattleShip(4);
			ships[2] = new Cruise(3);
			ships[3] = new Submarine(3);
			ships[4] = new Destroyer(2);
		}
		return ships;
	};

	/**
	 * Shot
	 */
	this.shot = function() {
		// Nop, only implemented in enemy.
	};

	/**
	 * Method to positioned ships randomly.
	 */
	this.positionedShipsRandomly = function() {
		ships = this.getShips(true);
		var i = 0;
		while (i < ships.length) {
			var randomCell = parseInt(Math.random() * map.getPanel().length);
			var isHorizontal = Math.round(Math.random() * 1) == 0 ? true
					: false;
			var shipLength = ships[i].getShipLength();
			// Check if ship is positioned horizontal or vertical.
			if (isHorizontal) {
				// calculate sectors.
				var firstSector = parseInt(randomCell * 0.1) * 10;
				firstSector = firstSector == 0 ? firstSector + 1 : firstSector;
				firstSector = firstSector % 20 == 0 ? firstSector
						: firstSector - 10;
				var lastSector = firstSector + 20;
				// Check if is possible put ship in a panel.
				if (randomCell + shipLength - 1 < lastSector) {
					// Put ships in panel if not are any.
					if (canPutShips(randomCell, shipLength, isHorizontal)) {
						var positions = [];
						for (var j = 0; j < shipLength; j++) {
							map.modifyBox(randomCell, "*");
							positions.push(randomCell);
							randomCell++;
						}
						ships[i].setPositions(positions);
						i++;
					}
				}
			} else {
				// Calculate sectors.
				var lastSectorCell = (shipLength - 1) * 20 + randomCell;
				var panel = map.getPanel();
				// Check if is possible put ship in a panel.
				if (lastSectorCell < panel.length) {
					// Put ships in panel if not are any.
					if (canPutShips(randomCell, shipLength, isHorizontal)) {
						var positions = [];
						for (var j = 0; j < shipLength; j++) {
							map.modifyBox(randomCell, "*");
							positions.push(randomCell);
							randomCell = randomCell + 20;
						}
						ships[i].setPositions(positions);
						i++;
					}
				}
			}
		}
	};

	// Private method
	var canPutShips = function(randomCell, shipLength, isHorizontal) {
		var panel = map.getPanel();
		var canPutShip = true;
		// Check if not are ships in a row.
		for (var j = 0; j < shipLength; j++) {
			if (panel[randomCell] == "#") {
				randomCell = isHorizontal ? randomCell + 1 : randomCell + 20;
			} else {
				canPutShip = false;
				break;
			}
		}
		// Put ship if you can.
		if (canPutShip) {
			return true;
		}
		return false;
	};
}
