/*
 * Copyright 2013 Adrián López González <adrian.lg.456@gmail.com>
 *
 * This file is part of BattleShips.
 *
 * BattleShips is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BattleShips is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BattleShips.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Map.js
 * 
 * @version 1.0
 * @author Adrián López González
 * 
 */

function Map() {

	/* Fields */
	var panel = new Array(201).join("#").split("");

	/**
	 * Get panel.
	 */
	this.getPanel = function() {
		return panel;
	};
	
	/**
	 * Set value in a position of panel.
	 */
	this.modifyBox = function(position, value) {
		panel[position] = value;
	};
	
	/**
	 * Reset panel.
	 */
	this.resetPanel = function() {
		panel = new Array(201).join("#").split("");
	};
}