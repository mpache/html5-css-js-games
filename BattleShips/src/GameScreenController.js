/*
 * Copyright 2013 Adrián López González <adrian.lg.456@gmail.com>
 *
 * This file is part of BattleShips.
 *
 * BattleShips is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * BattleShips is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with BattleShips.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * GameScreenController.js
 * 
 * @version 1.0
 * @author Adrián López González
 * 
 */
var controller;

function GameScreenController() {

	/* Fields */
	var currentScreen = new SplashScreen(this);
	var nextScreen = currentScreen;

	/**
	 * Render game.
	 */
	this.render = function() {
		if (currentScreen != nextScreen) {
			currentScreen = nextScreen;
		}
		currentScreen.animation();
	};

	/**
	 * Set next screen.
	 */
	this.setNextScreen = function(nextS) {
		nextScreen = nextS;
	};

	/**
	 * Get if screen animation is paused.
	 */
	this.isAnimationComplete = function() {
		return currentScreen.getIsAnimationComplete();
	};
}

/**
 * Initialized application
 */
function init() {
	if (controller == null)
		controller = new GameScreenController();
	// Render	
	controller.render();
	setTimeout('init()', 50);
}

window.onload = init;
