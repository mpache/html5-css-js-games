/*
 * Copyright 2014 Adrián López González <adrian.lg.456@gmail.com>
 *
 * This file is part of AirBattleOnline.
 *
 * AirBattleOnline is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AirBattleOnline is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AirBattleOnline.  If not, see <http://www.gnu.org/licenses/>.
 */

var __globalFields = {
    // container
    container : null,
    // Scene, camera and renderer.
    scene : null,
    camera : null,
    renderer : null,
    // constrols
    controls : null,
    // Logger fps
    fps : null
};

/**
 * Init.
 */
function init() {
    // Put fps logger.
    __globalFields.fps = new Stats();
    __globalFields.fps.domElement.style.position = 'absolute';
    __globalFields.fps.domElement.style.zIndex = 9999;

    // Create scene
    __globalFields.scene = new THREE.Scene();

    // Create camera.
    var SCREEN_WIDTH = window.innerWidth;
    var SCREEN_HEIGHT = window.innerHeight;
    var VIEW_ANGLE = 45;
    var ASPECT = SCREEN_WIDTH / SCREEN_HEIGHT;
    var NEAR = 0.1;
    var FAR = 20000;
    __globalFields.camera = new THREE.PerspectiveCamera(VIEW_ANGLE, ASPECT,
	    NEAR, FAR);

    // Add camera to scene
    __globalFields.scene.add(__globalFields.camera);
    __globalFields.camera.position.set(0, 150, 400);
    __globalFields.camera.lookAt(__globalFields.scene.position);

    // Create a webgl renderer.
    __globalFields.renderer = new THREE.WebGLRenderer({
	antialias : true
    });
    __globalFields.renderer.setSize(SCREEN_WIDTH, SCREEN_HEIGHT);

    // Get 3D context and add renderer
    var context3D = document.getElementById("context3D");
    context3D.appendChild(__globalFields.fps.domElement);
    context3D.appendChild(__globalFields.renderer.domElement);

    // LIGHT
//    var light = new THREE.PointLight(0xffffff);
//    light.position.set(0, 250, 0);
//    __globalFields.scene.add(light);

    // Create sky box
    var imagePrefix = "images/nebula-";
    var directions = [ "xpos", "xneg", "ypos", "yneg", "zpos", "zneg" ];
    var imageSuffix = ".png";
    var skyGeometry = new THREE.CubeGeometry(10000, 10000, 10000);
    var imageURLs = [];
    for (var i = 0; i < 6; i++)
	imageURLs.push(imagePrefix + directions[i] + imageSuffix);
    var textureCube = THREE.ImageUtils.loadTextureCube(imageURLs);
    var shader = THREE.ShaderLib["cube"];
    shader.uniforms["tCube"].value = textureCube;
    var skyMaterial = new THREE.ShaderMaterial({
	fragmentShader : shader.fragmentShader,
	vertexShader : shader.vertexShader,
	uniforms : shader.uniforms,
	depthWrite : false,
	side : THREE.BackSide
    });
    var skyBox = new THREE.Mesh(skyGeometry, skyMaterial);
    __globalFields.scene.add(skyBox);

    // controls
    __globalFields.controls = new THREE.TrackballControls(
	    __globalFields.camera, __globalFields.renderer.domElement);
}

function animate() {
    requestAnimationFrame(animate);
    render();
    update();
}

function update() {
    // if (keyboard.pressed("z")) {
    // // do something
    // }
    //
    // earthSphere.rotation.y += 0.0010;
    // cloudSphere.rotation.y += 0.0008;
    __globalFields.controls.update();
    __globalFields.fps.update();
}

function render() {
    __globalFields.renderer.clearColor();
    __globalFields.renderer.clearDepth();
    __globalFields.renderer.clearStencil();
    __globalFields.renderer.render(__globalFields.scene, __globalFields.camera);
}

/**
 * Initialized application.
 */
window.onload = function() {
    if (!Detector.webgl) {
	document.write("Your browser not support webgl or is not activated...");
    } else {
	init();
	animate();
    }
};
