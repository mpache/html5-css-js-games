/*
 * Copyright 23/01/2014 Adrián López González <adrian.lg.456@gmail.com>
 *
 * This file is part of TennisJavascript.
 *
 * TennisJavascript is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * TennisJavascript is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with TennisJavascript.  If not, see <http://www.gnu.org/licenses/>.
 */

/* Fields */
var timer;
var isSinglePlayer = true;
var screenWidth = innerWidth;
var screenHeight = innerHeight;
// Points
var leftPoints = 0;
var rightPoints = 0;
// Balls
var balls = new Array();
// Paddles
var paddles = new Array();

// =========
// OBJECTS
// =========
var Ball = function(radius) {
	/* Fields */
	this.posX = screenWidth / 2;
	this.posY = (screenHeight + 100) / 2;
	this.radius = radius;
	// Variables to move ball in axis.
	this.moveX = Math.random() * 4 + 1;
	this.moveY = Math.random() * 4 + 1;
};

var Paddle = function(pos, paddleWidth, paddleHeight) {
	/* Fields */
	this.width = paddleWidth;
	this.height = paddleHeight;
	this.posX = pos === 'left' ? 0 : screenWidth - this.width;
	this.posY = screenHeight / 2 - this.height / 2;
};

// ==========
// FUNCTIONS
// ==========
function changeTheme(op) {
	var colorFont, theme;
	switch (op) {
	case 2:
		theme = "theme2";
		colorFont = "colorFont2";
		break;
	case 3:
		theme = "theme3";
		colorFont = "colorFont3";
		break;
	default:
		theme = "theme1";
		colorFont = "colorFont1";
		break;
	}
	// Change theme
	document.getElementById("top").className = "font " + colorFont;
	document.getElementById("separator1").className = "separator " + theme;
	document.getElementById("separator2").className = "separator " + theme;
	document.getElementById("paddleLeft").className = theme;
	document.getElementById("paddleRight").className = theme;
}

function addBall() {
	var ball = document.createElement("div");
	ball.id = "ball_" + balls.length;
	ball.className = "ball";
	document.getElementById("board").appendChild(ball);
	balls.push(new Ball(10));
}

// ============
// COLLISIONS
// ============
function thereCollisionBallPaddle(paddle, ball) {
	// Check if ball is Yaxis of paddle
	if (ball.posY + ball.radius >= paddle.posY
			&& ball.posY - ball.radius <= paddle.posY + paddle.height) {
		// Check if ball touch paddle right.
		if (ball.posX >= (paddle.posX - paddle.width) && paddle.posX > 0) {
			return true;
		}
		// Check if ball touch paddle left.
		else if (ball.posX <= paddle.width && paddle.posX == 0) {
			return true;
		}
		// Ball not collide by the paddles.
		else {
			return false;
		}
	}
}

// ===========
// DRAW
// ===========
function draw() {
	// Update points.
	document.getElementById("scoreIA").innerHTML = leftPoints;
	document.getElementById("scorePlayer").innerHTML = rightPoints;
	// Balls
	for (var i = 0; i < balls.length; i++) {
		document.getElementById("ball_" + i).style.width = balls[i].radius
				+ "px";
		document.getElementById("ball_" + i).style.height = balls[i].radius
				+ "px";
		document.getElementById("ball_" + i).style.marginTop = balls[i].posY
				+ "px";
		document.getElementById("ball_" + i).style.marginLeft = balls[i].posX
				+ "px";
	}

	// Paddle left
	document.getElementById("paddleLeft").style.width = paddles[0].width + "px";
	document.getElementById("paddleLeft").style.height = paddles[0].height
			+ "px";
	if (paddles[0].posY <= 0) {
		document.getElementById("paddleLeft").style.marginTop = "0px";
	} else if (paddles[0].posY >= screenHeight - 150) {
		document.getElementById("paddleLeft").style.marginTop = screenHeight
				- paddles[0].height + "px";
	} else {
		document.getElementById("paddleLeft").style.marginTop = paddles[0].posY
				+ "px";
	}
	// Paddle right
	document.getElementById("paddleRight").style.width = paddles[1].width
			+ "px";
	document.getElementById("paddleRight").style.height = paddles[1].height
			+ "px";
	if (paddles[1].posY <= 0) {
		document.getElementById("paddleRight").style.marginTop = "0px";
	} else if (paddles[1].posY >= screenHeight - 150) {
		document.getElementById("paddleRight").style.marginTop = screenHeight
				- paddles[1].height + "px";
	} else {
		document.getElementById("paddleRight").style.marginTop = paddles[1].posY
				+ "px";
	}
}

// ===========
// UPDATE
// ===========
function update() {
	draw();

	// Move the paddle controlled by IA (Left).
	if (isSinglePlayer) {
		paddles[0].posY = balls[0].posY - paddles[0].height / 2;
	}

	for (var i = 0; i < balls.length; i++) {
		// Move the ball.
		balls[i].posX += balls[i].moveX;
		balls[i].posY += balls[i].moveY;

		// Check if there are a collision of paddles with ball.
		if (thereCollisionBallPaddle(paddles[0], balls[i])) {
			balls[i].moveX = -balls[i].moveX;
		} else if (thereCollisionBallPaddle(paddles[1], balls[i])) {
			balls[i].moveX = -Math.random() * 4 + 1;
		} else {
			// Check the collision with screen height.
			if (balls[i].posY + balls[i].radius > screenHeight) {
				balls[i].moveY = -balls[i].moveY;
				balls[i].posY = screenHeight - balls[i].radius;
			} else if (balls[i].posY - balls[i].radius <= 0) {
				balls[i].moveY = -balls[i].moveY;
				balls[i].posY = balls[i].radius;
			}
			// If not are a collision, add points to user or ia.
			if (balls[i].posX + balls[i].radius > screenWidth) {
				balls[i] = new Ball(10);
				leftPoints++;
			} else if (balls[i].posX < 0) {
				balls[i] = new Ball(10);
				rightPoints++;
			}
		}
	}

	// Re call.
	timer = setTimeout('update()', 10);
}

/**
 * Move right paddle.
 */
window.onmousemove = function(e) {
	paddles[1].posY = e.pageY - paddles[1].height / 2;
};

/**
 * Move left paddle and check other keys.
 */
window.onkeydown = function(e) {
	// Move left paddle if not is a single player.
	if (!isSinglePlayer) {
		if (e.which == 87) {
			paddles[0].posY = paddles[0].posY >= 0 ? paddles[0].posY - 10 : 0;
		} else if (e.which == 83) {
			paddles[0].posY = paddles[0].posY <= screenHeight - 150 ? paddles[0].posY + 10
					: screenHeight - paddles[1].height;
		}
	}
	// Pause or resume game.
	if (e.which == 80) {
		document.getElementById("pauseGame").style.display = "block";
		clearTimeout(timer);
	} else if (e.which == 82) {
		document.getElementById("pauseGame").style.display = "none";
		update();
	} else if (e.which == 113) {
		addBall();
	}
	// Change theme
	else if (e.which == 49) {
		changeTheme(1);
	} else if (e.which == 50) {
		changeTheme(2);
	} else if (e.which == 51) {
		changeTheme(3);
	}
};

// ======
// MAIN
// ======
/**
 * Initialized application.
 */
window.onload = function() {
	window.onresize();
	// Create paddles.
	var paddleLeft = new Paddle("left", 20, 150);
	var paddleRight = new Paddle("right", 20, 150);
	paddles.push(paddleLeft);
	paddles.push(paddleRight);
	// Create ball.
	addBall();
	// Create events.
	document.getElementById("singlePlayer").onclick = function() {
		isSinglePlayer = true;
		this.parentNode.style.display = "none";
		update();
	};
	document.getElementById("multiPlayer").onclick = function() {
		isSinglePlayer = false;
		this.parentNode.style.display = "none";
		update();
	};
	// Show or close how to play.
	document.getElementById("options").onclick = function() {
		this.parentNode.style.display = "none";
		document.getElementById("howToPlay").style.display = "block";
	};
	document.getElementById("closeHowToPlay").onclick = function() {
		this.parentNode.style.display = "none";
		document.getElementById("gameLayer").style.display = "block";
	};
};

/**
 * Resize elements.
 */
window.onresize = function() {
	screenWidth = innerWidth;
	screenHeight = innerHeight - 70;
	document.getElementById("board").style.height = innerHeight - 70 + "px";
};