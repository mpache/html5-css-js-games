/*
 * Copyright 2014 Adrián López González <adrian.lg.456@gmail.com>
 *
 * This file is part of AirBattle2.
 *
 * AirBattle2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AirBattle2 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AirBattle2.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Enum class.
 */
function EnumDirections() {
	throw new Error("This class is a enum class, can't be instanciated.");
};
EnumDirections.upLeft = "UPLEFT";
EnumDirections.upRight = "UPRIGHT";
EnumDirections.downLeft = "DOWNLEFT";
EnumDirections.downRight = "DOWNRIGHT";
EnumDirections.up = "UP";
EnumDirections.left = "LEFT";
EnumDirections.right = "RIGHT";
EnumDirections.down = "DOWN";
