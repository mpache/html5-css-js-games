/*
 * Copyright 2014 Adrián López González <adrian.lg.456@gmail.com>
 *
 * This file is part of AirBattle2.
 *
 * AirBattle2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AirBattle2 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AirBattle2.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Define Player.
 * 
 * @param x
 *            is the initial posX in the screen.
 * @param y
 *            is the initial posY in the screen.
 * @param degrees
 *            is the degree to rotate player.
 */
function Player(x, y, degrees) {

	/* Constants */
	var SPEED = 10;
	var DAMAGE = 10;
	this.WIDTH = 100;
	this.HEIGHT = 100;

	/* Fields */
	var player = null;
	var lifeBar = null;
	// Positions and limit screen.
	var posX = x;
	var posY = y;
	var limitX = innerWidth;
	var limitY = innerHeight;
	// life.
	var life = 100;
	// Transform
	var rotate = degrees;
	// Move field.
	var directionMove = "";
	// Shoots
	var poolShoots = [];

	/**
	 * Player constructor.
	 */
	(function() {
		player = document.createElement("div");
		player.setAttribute("class", "player");
		if (degrees == 0) {
			player.setAttribute("id", "player1");
		} else {
			player.setAttribute("id", "player2");
		}
		// Create life.
		lifeBar = document.createElement("div");
		lifeBar.setAttribute("class", "lifeBar");	
		var hud = document.createElement("img");
		hud.setAttribute("class", "hud");
		hud.setAttribute("src", "resources/hud.png");	
		lifeBar.appendChild(hud);
		player.appendChild(lifeBar);
		// Add player to body.
		document.body.appendChild(player);

	}());

	/**
	 * Update player.
	 */
	this.update = function() {
		move(this);
		draw(this);
		// Update shoots.
		for (var i = 0; i < poolShoots.length; i++) {
			poolShoots[i].update();
			if (poolShoots[i].getRange() == 0) {
				poolShoots.splice(i, 1);
			}
		}
	};

	/**
	 * Shoot.
	 */
	this.shoot = function() {
		if (directionMove != "") {
			var px = posX;
			var py = posY;
			if (directionMove == EnumDirections.right) {
				px += this.WIDTH - 20;
				py += Math.floor(this.HEIGHT / 2);
			} else if (directionMove == EnumDirections.left) {
				px -= 20;
				py += Math.floor(this.HEIGHT / 2);
			} else if (directionMove == EnumDirections.up) {
				px += Math.floor(this.WIDTH / 2);
				py -= Math.floor(this.HEIGHT / 2) - 20;
			} else if (directionMove == EnumDirections.down) {
				// px += Math.floor(this.WIDTH / 2) - 45;
				py += Math.floor(this.HEIGHT / 2) + 30;
			} else if (directionMove == EnumDirections.upLeft) {
				px -= Math.floor(this.WIDTH / 2) - 30;
				py -= Math.floor(this.HEIGHT / 2) - 70;
			} else if (directionMove == EnumDirections.upRight) {
				px += Math.floor(this.WIDTH / 2) + 30;
				py -= Math.floor(this.HEIGHT / 2) - 70;
			} else if (directionMove == EnumDirections.downLeft) {
				px += Math.floor(this.WIDTH / 2) - 40;
				py += Math.floor(this.HEIGHT / 2) + 20;
			} else if (directionMove == EnumDirections.downRight) {
				px += Math.floor(this.WIDTH / 2) - 10;
				py += Math.floor(this.HEIGHT / 2) + 30;
			}
			poolShoots.push(new Shoot(px, py, directionMove));
		}
	};

	/**
	 * Check if this player is dead.
	 * 
	 * @param poolShoots
	 *            is a poolShoots to determine if this player has been touched.
	 * 
	 * @returns true if player is dead, false otherwise.
	 */
	this.isDead = function(poolShoots) {
		for (var i = 0; i < poolShoots.length; i++) {
			var px = posX;
			var py = posY;
			var width = px + this.WIDTH;
			var height = py + this.HEIGHT;
			// Check if is damaged.
			if (poolShoots[i].getPosX() >= px
					&& poolShoots[i].getPosX() <= width
					&& poolShoots[i].getPosY() >= py
					&& poolShoots[i].getPosY() <= height) {
				// Remove life
				life -= DAMAGE;
				// Remove shoot.
				poolShoots[i].remove();
				if (poolShoots[i].getRange() == 0) {
					poolShoots.splice(i, 1);
				}
				// Check if user is dead.
				if (life <= 0) {
					life = 100;
					// Calculate a random position.
					directionMove = "";
					posX = Math.floor(Math.random() * limitX + 1);
					posY = Math.floor(Math.random() * limitY + 1);
					return true;
				}
			}
		}
		return false;
	};

	// PRIVATE METHODS
	var draw = function(context) {
		player.style.marginLeft = posX + "px";
		player.style.marginTop = posY + "px";
		// Draw life
		lifeBar.style.width = life + "px";
		// Rotate player.
		player.style.transform = "rotateY(" + rotate + "deg)";
		player.style.webkitTransform = "rotateY(" + rotate + "deg)";
	};

	var move = function(context) {
		switch (directionMove) {
		case EnumDirections.up:
			posY -= SPEED;
			break;
		case EnumDirections.left:
			posX -= SPEED;
			rotate = 180;
			break;
		case EnumDirections.down:
			posY += SPEED;
			break;
		case EnumDirections.right:
			posX += SPEED;
			rotate = 0;
			break;
		case EnumDirections.upLeft:
			posY -= SPEED;
			posX -= SPEED;
			rotate = 180;
			break;
		case EnumDirections.upRight:
			posY -= SPEED;
			posX += SPEED;
			rotate = 0;
			break;
		case EnumDirections.downLeft:
			posY += SPEED;
			posX -= SPEED;
			rotate = 180;
			break;
		case EnumDirections.downRight:
			posY += SPEED;
			posX += SPEED;
			rotate = 0;
			break;
		default:
			// nop.
		}
		// Check if position is in limits.
		posX = posX < 0 ? limitX : posX;
		posX = posX > limitX ? 0 : posX;
		posY = posY < 0 ? limitY : posY;
		posY = posY > limitY ? 0 : posY;
	};

	// Getters & Setters
	this.getPosX = function() {
		return posX;
	};

	this.getPosY = function() {
		return posY;
	};

	this.getPoolShoots = function() {
		return poolShoots;
	};

	this.setLimitX = function(newLimitX) {
		limitX = newLimitX;
	};

	this.setLimitY = function(newLimitY) {
		limitY = newLimitY;
	};

	this.setDirectionMove = function(direction) {
		directionMove = direction;
	};
}
