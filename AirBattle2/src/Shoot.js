/*
 * Copyright 2014 Adrián López González <adrian.lg.456@gmail.com>
 *
 * This file is part of AirBattle2.
 *
 * AirBattle2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AirBattle2 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AirBattle2.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Define shoot
 * 
 * @param pX
 *            is the initial position in x axis.
 * @param pY
 *            is the initial position in y axis.
 * @param direction
 *            is the direction of shoot.
 */
function Shoot(pX, pY, direction) {

	/* Constants */
	var SPEED = 20;
	var RANGE_MAX = 12;

	/* Fields */
	var shoot = null;
	// Positions.
	var posX = pX;
	var posY = pY;
	// Transform.
	var rotate = 0;
	// Range, is the actual range of this shoot.
	var range = 0;

	/**
	 * Shoot constructor.
	 */
	(function() {
		shoot = document.createElement("div");
		shoot.setAttribute("class", "shoot");
		document.body.appendChild(shoot);
		// Initial position.
		shoot.style.marginLeft = posX + "px";
		shoot.style.marginTop = posY + "px";
		shoot.style.transform = "rotate(" + rotate + "deg)";
		shoot.style.webkitTransform = "rotate(" + rotate + "deg)";
	}());

	/**
	 * Update shoot.
	 */
	this.update = function() {
		move(this);
		draw(this);
	};

	/**
	 * Remove shoot.
	 */
	this.remove = function() {
		range = 0;
		shoot.parentNode.removeChild(shoot);
	};

	// PRIVATE METHODS
	var draw = function(context) {
		if (range < RANGE_MAX) {
			shoot.style.marginLeft = posX + "px";
			shoot.style.marginTop = posY + "px";
			shoot.style.transform = "rotateY(" + rotate + "deg)";
			shoot.style.webkitTransform = "rotateY(" + rotate + "deg)";
			range++;
		} else {
			context.remove();
		}
	};

	var move = function(context) {
		switch (direction) {
		case EnumDirections.up:
			posY -= SPEED;
			break;
		case EnumDirections.left:
			posX -= SPEED;
			rotate = 180;
			break;
		case EnumDirections.down:
			posY += SPEED;
			break;
		case EnumDirections.right:
			posX += SPEED;
			rotate = 0;
			break;
		case EnumDirections.upLeft:
			posY -= SPEED;
			posX -= SPEED;
			rotate = 180;
			break;
		case EnumDirections.upRight:
			posY -= SPEED;
			posX += SPEED;
			rotate = 0;
			break;
		case EnumDirections.downLeft:
			posY += SPEED;
			posX -= SPEED;
			rotate = 180;
			break;
		case EnumDirections.downRight:
			posY += SPEED;
			posX += SPEED;
			rotate = 0;
			break;
		default:
			// nop.
		}
	};

	// Getters & Setters
	this.getPosX = function() {
		return posX;
	};

	this.getPosY = function() {
		return posY;
	};

	this.getRange = function() {
		return range;
	};
}