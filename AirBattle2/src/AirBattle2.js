/*
 * Copyright 2014 Adrián López González <adrian.lg.456@gmail.com>
 *
 * This file is part of AirBattle2.
 *
 * AirBattle2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * AirBattle2 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AirBattle2.  If not, see <http://www.gnu.org/licenses/>.
 */

/* Global Fields */
var __globalFields = {
	// Define players
	player1 : null,
	plauer2 : null,
	// Variables to count down.
	maxCount : 15,
	actualCount : 0,
	// Create a literal object to handler multiples keys in 'onKeyDown'.
	mapMultipleKeys : {},
};

/**
 * Function to create count down.
 */
function countDown() {
	var countToStartGame = document.getElementById("countToStartGame");
	countToStartGame.style.display = "block";
	if (__globalFields.actualCount == __globalFields.maxCount) {
		var count = countToStartGame.innerHTML;
		if (count > 1) {
			count--;
			countToStartGame.innerHTML = count;
		} else if (count == 1) {
			count--;
			countToStartGame.innerHTML = "Fight!";
		} else {
			__globalFields.isGameStart = true;
			countToStartGame.style.display = "none";
		}
		__globalFields.actualCount = 0;
		countToStartGame.style.fontSize = "80px";
	} else {
		var fontSize = parseInt(countToStartGame.style.fontSize);
		fontSize = isNaN(fontSize) ? 80 : fontSize;
		fontSize++;
		countToStartGame.style.fontSize = fontSize + "px";
		__globalFields.actualCount++;
	}
	// Check if game is start.
	if (__globalFields.isGameStart) {
		render();
	} else {
		__globalFields.player1.update();
		__globalFields.player2.update();
		setTimeout('countDown()', 30);
	}
}

/**
 * Render screen.
 */
function render() {
	var player1 = __globalFields.player1;
	var player2 = __globalFields.player2;
	// Update players.
	player1.update();
	player2.update();
	// Update score
	var score = 0;
	if (player1.isDead(player2.getPoolShoots())) {
		score = document.getElementById("score2");
		score.innerHTML = score.innerHTML * 1 + 1;
	}
	if (player2.isDead(player1.getPoolShoots())) {
		score = document.getElementById("score1");
		score.innerHTML = score.innerHTML * 1 + 1;
	}
	// render.
	setTimeout('render()', 30);
}

// ========================
// EVENTS
// ========================
/**
 * Initialized application.
 */
window.onload = function() {
	// PLAY event.
	document.getElementById("bPlay").onclick = function() {
		document.getElementById("title").style.display = "none";
		document.getElementById("mainMenu").style.display = "none";
		document.getElementById("scores").style.display = "block";
		// Create random position.
		var randomPLeft = Math.floor(Math.random() * innerWidth / 2);
		var randomPRight = Math.floor(Math.random() * innerWidth / 2
				+ (innerWidth / 2) - 80);
		var randomPHeight1 = Math.floor(Math.random() * innerHeight / 2);
		var randomPHeight2 = Math.floor(Math.random() * innerHeight / 2);
		// Create a players.
		__globalFields.player1 = new Player(randomPLeft, randomPHeight1, 0);
		__globalFields.player2 = new Player(randomPRight, randomPHeight2, 180);
		// Call on resize for first time.
		window.onresize();
		// Initialized.
		__globalFields.player1.update();
		__globalFields.player2.update();
		countDown();
	};
	// HOW TO PLAY event.
	document.getElementById("bHowToPlay").onclick = function() {
		document.getElementById("mainMenu").style.display = "none";
		document.getElementById("menuHowToPlay").style.display = "block";

	};
	document.getElementById("bCloseHowToPlay").onclick = function() {
		document.getElementById("mainMenu").style.display = "block";
		document.getElementById("menuHowToPlay").style.display = "none";
	};
	// CREDITS event
	document.getElementById("bOpenCredits").onclick = function() {
		document.getElementById("mainMenu").style.display = "none";
		document.getElementById("menuCredits").style.display = "block";

	};
	document.getElementById("bCloseCredits").onclick = function() {
		document.getElementById("mainMenu").style.display = "block";
		document.getElementById("menuCredits").style.display = "none";
	};
	// Call on resize for first time.
	window.onresize();
};

/**
 * Resize screen.
 */
window.onresize = function() {
	// Resize board.
	document.getElementById("board").style.height = innerHeight - 20 + "px";
	// Resize limits of players.
	var player1 = __globalFields.player1;
	var player2 = __globalFields.player2;
	if (player1 != null && player2 != null) {
		// Change limits of players.
		player1.setLimitX(innerWidth - player1.WIDTH - 8);
		player1.setLimitY(innerHeight - player1.HEIGHT - 20);
		player2.setLimitX(innerWidth - player1.WIDTH - 8);
		player2.setLimitY(innerHeight - player1.HEIGHT - 20);
	}
};

/**
 * If user click screen change players.
 */
window.onmousedown = function(e) {
	if (e.button == 2) {
		// Change players.
		var aux = __globalFields.player1;
		__globalFields.player1 = __globalFields.player2;
		__globalFields.player2 = aux;
	}
};

/**
 * Move players.
 */
window.onkeydown = function(e) {
	var player1 = __globalFields.player1;
	var player2 = __globalFields.player2;
	var mapMultipleKeys = __globalFields.mapMultipleKeys;
	// var e = window.event || e;
	mapMultipleKeys[e.which] = true;
	// ===========
	// Player 1.
	// ===========
	// w = 87, a = 65, s = 83, d = 68
	// DIAGONALS
	// UP Left
	if (mapMultipleKeys[87] && mapMultipleKeys[65]) {
		player1.setDirectionMove(EnumDirections.upLeft);
	}
	// Up Right
	else if (mapMultipleKeys[87] && mapMultipleKeys[68]) {
		player1.setDirectionMove(EnumDirections.upRight);
	}
	// Down Left
	else if (mapMultipleKeys[83] && mapMultipleKeys[65]) {
		player1.setDirectionMove(EnumDirections.downLeft);
	}
	// Down Right
	else if (mapMultipleKeys[83] && mapMultipleKeys[68]) {
		player1.setDirectionMove(EnumDirections.downRight);
	}
	// NORMAL DIRECTIONS
	// Up
	else if (mapMultipleKeys[87]) {
		player1.setDirectionMove(EnumDirections.up);
	}
	// Left
	else if (mapMultipleKeys[65]) {
		player1.setDirectionMove(EnumDirections.left);
	}
	// Down
	else if (mapMultipleKeys[83]) {
		player1.setDirectionMove(EnumDirections.down);
	}
	// Right
	else if (mapMultipleKeys[68]) {
		player1.setDirectionMove(EnumDirections.right);
	}
	// shoot
	if (mapMultipleKeys[32]) {
		if (__globalFields.isGameStart)
			player1.shoot();
	}
	// ===========
	// Player 2.
	// ===========
	// upArrow = 38, leftArrow = 37, downArrow = 40, rightArrow = 39
	// DIAGONALS
	// UP Left
	if (mapMultipleKeys[38] && mapMultipleKeys[37]) {
		player2.setDirectionMove(EnumDirections.upLeft);
	}
	// Up Right
	else if (mapMultipleKeys[38] && mapMultipleKeys[39]) {
		player2.setDirectionMove(EnumDirections.upRight);
	}
	// Down Left
	else if (mapMultipleKeys[40] && mapMultipleKeys[37]) {
		player2.setDirectionMove(EnumDirections.downLeft);
	}
	// Down Right
	else if (mapMultipleKeys[40] && mapMultipleKeys[39]) {
		player2.setDirectionMove(EnumDirections.downRight);
	}
	// NORMAL DIRECTIONS
	// Up
	else if (mapMultipleKeys[38]) {
		player2.setDirectionMove(EnumDirections.up);
	}
	// Left
	else if (mapMultipleKeys[37]) {
		player2.setDirectionMove(EnumDirections.left);
	}
	// Down
	else if (mapMultipleKeys[40]) {
		player2.setDirectionMove(EnumDirections.down);
	}
	// Right
	else if (mapMultipleKeys[39]) {
		player2.setDirectionMove(EnumDirections.right);
	}
	// Shoot
	if (mapMultipleKeys[96]) {
		if (__globalFields.isGameStart)
			player2.shoot();
	}
};

/**
 * Reset map of multiples keys.
 */
window.onkeyup = function(e) {
	var mapMultipleKeys = __globalFields.mapMultipleKeys;
	mapMultipleKeys[e.which] = false;
};